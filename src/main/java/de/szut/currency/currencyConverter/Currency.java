package de.szut.currency.currencyConverter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {
    @JsonProperty
    private String base;
    @JsonProperty
    private String date;
    //@JsonProperty
    //private Rates rates;
    private HashMap<String, Double> rates;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Currency{");
        sb.append("base='").append(base).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append(", rates=").append(rates);
        sb.append('}');
        return sb.toString();
    }
}
