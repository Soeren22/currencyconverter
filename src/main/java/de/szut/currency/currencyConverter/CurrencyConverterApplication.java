package de.szut.currency.currencyConverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;

@SpringBootApplication
public class CurrencyConverterApplication {
	public static void main(String[] args) {

		SpringApplication.run(CurrencyConverterApplication.class, args);
		CurrencyService service = new CurrencyService(new RestTemplateBuilder());
		Currency result= service.getLatestEuroRates();
		System.out.println(result);
		System.out.println(service.convert("EUR", "USD", 50.0));
	}
}
