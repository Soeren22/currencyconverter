package de.szut.currency.currencyConverter;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CurrencyService {

    private RestTemplate restTemplate;

    public CurrencyService(RestTemplateBuilder restTemplateBuilder)
    {
        this.restTemplate = restTemplateBuilder.build();
    }

    private String url = "https://api.exchangerate.host/latest";

    public Currency getLatestEuroRates(){
        return this.restTemplate.getForObject(url, Currency.class);
    }

    public String convert(String from, String to, Double amount){
        return this.restTemplate.getForObject("https://api.exchangerate.host/convert?from={from}&to={to}&amount={amount}", String.class, from, to, amount );
    }
}
